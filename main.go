package main

import (
	"AutoGrader/utils"
	"bytes"
	"fmt"
	"os/exec"
	"strings"
	"time"

	"golang.org/x/exp/slices"
)

//folder names
var (
	SUBMISSION_FOLDER_NAME     = "submissions"
	HARNESS_FOLDER_NAME        = "harness"
	TEMP_GRADING_FOLDER_NAME   = "temp_grading"
	GRADING_OUTPUT_FOLDER_NAME = "report"
)

//settings
var (
	//the ext of each item inside SUBMISSION_FOLDER_NAME folder that should be graded
	SUBMISSION_ITEM_EXT = "tar"
	//the ext of the other generated file inside TEMP_GRADING_FOLDER_NAME during the grading process
	OTHER_FILES_TO_INCLUDE_EXT = "txt"
	GRADE_COMMAND              = "bash"
	// %temp% to annotate temp grading GRADING_OUTPUT_FOLDER_NAME
	// %file% to annotate current grading file
	GRADE_ARGS  = []string{"gradeC02.sh", "%file%"}
	TIMEOUT_SEC = 10
)

func main() {
	//loop each submission file
	for index, submission_file := range utils.GetFileNamesFromDirectory(SUBMISSION_FOLDER_NAME, false) {
		if strings.HasSuffix(submission_file, SUBMISSION_ITEM_EXT) {
			index = index + 1
			fmt.Printf("%d - %s\n", index, submission_file)
			utils.ClearFolder(TEMP_GRADING_FOLDER_NAME)

			//copy files
			harness_files := utils.GetFileNamesFromDirectory(HARNESS_FOLDER_NAME, false)
			for _, harness_file := range harness_files {
				utils.CopyFile(HARNESS_FOLDER_NAME+"/"+harness_file, TEMP_GRADING_FOLDER_NAME+"/"+harness_file)
			}

			utils.CopyFile(SUBMISSION_FOLDER_NAME+"/"+submission_file, TEMP_GRADING_FOLDER_NAME+"/"+submission_file)

			utils.PrepareFolder(GRADING_OUTPUT_FOLDER_NAME)
			var student_folder = GRADING_OUTPUT_FOLDER_NAME + "/" + utils.GetFileNameWithoutExt(submission_file)

			//generate stdout for this submission
			utils.WriteToFile(student_folder+"/"+"stdout.txt", RunCurrent(student_folder, submission_file))

			//copy other generated files to the folder
			for _, grading_file := range utils.GetFileNamesFromDirectory(TEMP_GRADING_FOLDER_NAME, false) {
				if !slices.Contains(harness_files, grading_file) && strings.HasSuffix(grading_file, OTHER_FILES_TO_INCLUDE_EXT) {
					utils.CopyFile(TEMP_GRADING_FOLDER_NAME+"/"+grading_file, student_folder+"/"+grading_file)
				}
			}
		}
	}

	//clean up the project
	utils.RemoveFolder(TEMP_GRADING_FOLDER_NAME)
}

func RunCurrent(student_folder string, file string) string {
	//prepare grading report folder
	utils.PrepareFolder(student_folder)

	// instantiate new command
	var GRADING_COMMAND = exec.Command(GRADE_COMMAND, GetARG(file)...)
	GRADING_COMMAND.Dir = TEMP_GRADING_FOLDER_NAME
	fmt.Println(GRADING_COMMAND)
	GRADING_COMMAND.Stdin = strings.NewReader("0")

	// get pipe to standard output
	stdout, err := GRADING_COMMAND.StdoutPipe()
	if err != nil {
		return "cmd.StdoutPipe() error: " + err.Error()
	}

	// start process via command
	if err := GRADING_COMMAND.Start(); err != nil {
		return "cmd.Start() error: " + err.Error()
	}

	// setup a buffer to capture standard output
	var buf bytes.Buffer

	// create a channel to capture any errors from wait
	done := make(chan error)
	go func() {
		if _, err := buf.ReadFrom(stdout); err != nil {
			panic("buf.Read(stdout) error: " + err.Error())
		}
		done <- GRADING_COMMAND.Wait()
	}()

	// block on select, and switch based on actions received
	select {
	case <-time.After(time.Duration(TIMEOUT_SEC) * time.Second):
		if err := GRADING_COMMAND.Process.Kill(); err != nil {
			return "failed to kill: " + err.Error()
		}
		return "timeout reached, process killed"
	case err := <-done:
		if err != nil {
			close(done)
			return "process done, with error: " + err.Error()
		}
		return "process completed: " + buf.String()
	}
}

func GetARG(file string) []string {
	result := []string{}
	for i := range GRADE_ARGS {
		var current = GRADE_ARGS[i]
		if strings.Contains(current, "%temp%") {
			current = strings.ReplaceAll(current, "%temp%", TEMP_GRADING_FOLDER_NAME)
		} else if strings.Contains(current, "%file%") {
			current = strings.ReplaceAll(current, "%file%", file)
		}
		result = append(result, current)
	}
	return result
}
