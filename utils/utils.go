package utils

import (
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func RemoveFolder(folder string) {
	if _, err := os.Stat(folder); !os.IsNotExist(err) {
		err := os.RemoveAll(folder)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func ClearFolder(folder string) {
	RemoveFolder(folder)
	PrepareFolder(folder)
}

func PrepareFolder(folder string) {
	if _, err := os.Stat(folder); os.IsNotExist(err) {
		if err := os.Mkdir(folder, os.ModePerm); err != nil {
			log.Fatal(err)
		}
	}
}

func WriteToFile(file string, content string) {
	err := os.WriteFile(file, []byte(content), 0644)
	if err != nil {
		log.Fatal(err)
	}
}

func GetFileNameWithoutExt(fileName string) string {
	return fileName[:len(fileName)-len(filepath.Ext(fileName))]
}

func GetFileNamesFromDirectory(directory string, includeSubDir bool) []string {
	var result []string
	files, err := ioutil.ReadDir(directory)
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		if includeSubDir || !file.IsDir() {
			result = append(result, file.Name())
		}
	}
	return result
}

func CopyFile(source string, dest string) {
	sourceFile, err := os.Open(source)
	if err != nil {
		log.Fatal(err)
	}
	defer sourceFile.Close()

	// Create new file
	newFile, err := os.Create(dest)
	if err != nil {
		log.Fatal(err)
	}
	defer newFile.Close()

	_, err = io.Copy(newFile, sourceFile)
	if err != nil {
		log.Fatal(err)
	}
}
